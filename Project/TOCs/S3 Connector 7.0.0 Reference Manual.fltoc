﻿<?xml version="1.0" encoding="utf-8"?>
<CatapultToc
  Version="1">
  <TocEntry
    Title="Print_Cover_Page"
    Link="/Content/Template Examples/E_Front Matter Topics/Print_Cover_Page.htm"
    BreakType="pageLayout"
    StartSection="false"
    PageNumberReset="continue"
    PageType="title"
    PageLayout="/Content/Resources/PageLayouts/8_5X11/Title8_5X11.flpgl"></TocEntry>
  <TocEntry
    Title="Contents"
    Link="/Content/Template Examples/E_Front Matter Topics/TOC-SDL.htm"
    StartSection="false"
    BreakType="chapter"
    PageNumberReset="reset"
    PageNumberFormat="lower-roman"
    PageNumber="1" />
  <TocEntry
    Title="Legal Notice"
    Link="/Content/Template Examples/E_Front Matter Topics/LegalNotice.htm"
    BreakType="chapter"
    PageNumberFormat="lower-roman"
    StartSection="false"
    PageNumberReset="continue"
    PageNumber="1"></TocEntry>
  <TocEntry
    Title="Publication History"
    Link="/Content/Template Examples/E_Front Matter Topics/Publication History.htm"
    BreakType="pageLayout"
    StartSection="false" />
  <TocEntry
    Title="Typographic Conventions"
    Link="/Content/Template Examples/E_Front Matter Topics/Typographic Conventions.htm"
    BreakType="chapter"
    PageNumberReset="continue"
    PageNumberFormat="lower-roman"
    StartSection="false"
    ReplaceMergeNode="false"
    SectionNumberReset="continue"
    VolumeNumberReset="same"
    ChapterNumberReset="continue"
    ComputeToc="false" />
  <TocEntry
    Title="Introduction"
    Link="/Content/1_Introduction/Introduction.htm"
    BreakType="chapter"
    PageNumberReset="reset"
    StartSection="false"
    PageNumber="1"
    PageNumberFormat="decimal"
    ChapterNumberReset="reset"
    ChapterNumber="1"
    SectionNumberReset="continue"
    VolumeNumberReset="same"></TocEntry>
  <TocEntry
    Title="Service Operations"
    Link="/Content/2_The APIs/Service Operations.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue"
    ChapterNumberReset="continue"
    ChapterNumber="2"
    SectionNumberReset="continue"
    VolumeNumberReset="same">
    <TocEntry
      Title="GET Service"
      Link="/Content/2_The APIs/GET Service.htm" />
  </TocEntry>
  <TocEntry
    Title="Bucket Operations"
    Link="/Content/2_The APIs/Bucket Operations.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue"
    SectionNumberReset="continue"
    VolumeNumberReset="same"
    ChapterNumberReset="continue">
    <TocEntry
      Title="DELETE Bucket"
      Link="/Content/2_The APIs/DELETE Bucket.htm" />
    <TocEntry
      Title="GET Bucket Versioning"
      Link="/Content/2_The APIs/GET Bucket Versioning.htm" />
    <TocEntry
      Title="GET Bucket Location"
      Link="/Content/2_The APIs/GET Bucket Location.htm" />
    <TocEntry
      Title="GET Bucket (List Objects)"
      Link="/Content/2_The APIs/GET Bucket (List Objects).htm" />
    <TocEntry
      Title="GET Bucket Object Versions"
      Link="/Content/2_The APIs/GET Bucket Object Versions.htm" />
    <TocEntry
      Title="HEAD Bucket"
      Link="/Content/2_The APIs/HEAD Bucket.htm" />
    <TocEntry
      Title="PUT Bucket"
      Link="/Content/2_The APIs/PUT Bucket.htm" />
    <TocEntry
      Title="PUT Bucket Versioning"
      Link="/Content/2_The APIs/PUT Bucket Versioning.htm" />
    <TocEntry
      Title="GET Bucket ACL"
      Link="/Content/2_The APIs/GET Bucket ACL.htm" />
    <TocEntry
      Title="PUT Bucket ACL"
      Link="/Content/2_The APIs/PUT Bucket ACL.htm" />
    <TocEntry
      Title="List Multipart Uploads"
      Link="/Content/2_The APIs/List Multipart Uploads.htm" />
    <TocEntry
      Title="Bucket Website Operations"
      Link="/Content/2_The APIs/Bucket Website Operations.htm"
      StartSection="false"></TocEntry>
    <TocEntry
      Title="PUT Bucket Website"
      Link="/Content/2_The APIs/PUT Bucket Website.htm" />
    <TocEntry
      Title="GET Bucket Website"
      Link="/Content/2_The APIs/GET Bucket Website.htm" />
    <TocEntry
      Title="DELETE Bucket Website"
      Link="/Content/2_The APIs/DELETE Bucket Website.htm" />
    <TocEntry
      Title="Bucket CORS Operations"
      Link="/Content/2_The APIs/Bucket CORS Operations.htm"
      StartSection="false"></TocEntry>
    <TocEntry
      Title="PUT Bucket CORS"
      Link="/Content/2_The APIs/PUT Bucket CORS.htm" />
    <TocEntry
      Title="GET Bucket CORS"
      Link="/Content/2_The APIs/GET Bucket CORS.htm" />
    <TocEntry
      Title="DELETE Bucket CORS"
      Link="/Content/2_The APIs/DELETE Bucket CORS.htm" />
  </TocEntry>
  <TocEntry
    Title="Object Operations"
    Link="/Content/2_The APIs/Object Operations.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue">
    <TocEntry
      Title="DELETE Object"
      Link="/Content/2_The APIs/DELETE Object.htm" />
    <TocEntry
      Title="DELETE Object Tagging"
      Link="/Content/2_The APIs/DELETE Object Tagging.htm" />
    <TocEntry
      Title="Multi-Object Delete"
      Link="/Content/2_The APIs/Multi-Object Delete.htm" />
    <TocEntry
      Title="GET Object"
      Link="/Content/2_The APIs/GET Object.htm" />
    <TocEntry
      Title="GET Object Tagging"
      Link="/Content/2_The APIs/GET Object Tagging.htm" />
    <TocEntry
      Title="GET Object ACL"
      Link="/Content/2_The APIs/GET Object ACL.htm"></TocEntry>
    <TocEntry
      Title="HEAD Object"
      Link="/Content/2_The APIs/HEAD Object.htm" />
    <TocEntry
      Title="PUT Object"
      Link="/Content/2_The APIs/PUT Object.htm" />
    <TocEntry
      Title="PUT Object Tagging"
      Link="/Content/2_The APIs/PUT Object Tagging.htm" />
    <TocEntry
      Title="PUT Object ACL"
      Link="/Content/2_The APIs/PUT Object ACL.htm" />
    <TocEntry
      Title="PUT Object - Copy"
      Link="/Content/2_The APIs/PUT Object - Copy.htm" />
    <TocEntry
      Title="Initiate Multipart Upload"
      Link="/Content/2_The APIs/Initiate Multipart Upload.htm"></TocEntry>
    <TocEntry
      Title="Upload Part"
      Link="/Content/2_The APIs/Upload Part.htm"></TocEntry>
    <TocEntry
      Title="Upload Part - Copy"
      Link="/Content/2_The APIs/Upload Part - Copy.htm" />
    <TocEntry
      Title="Complete Multipart Upload"
      Link="/Content/2_The APIs/Complete Multipart Upload.htm" />
    <TocEntry
      Title="Abort Multipart Upload"
      Link="/Content/2_The APIs/Abort Multipart Upload.htm" />
    <TocEntry
      Title="List Parts"
      Link="/Content/2_The APIs/List Parts.htm"></TocEntry>
  </TocEntry>
  <TocEntry
    Title="Service Utilization API (UTAPI) Operations"
    Link="/Content/3_Service Utilization API/Service Utilization API.htm"
    conditions="PrintGuides.GA6.2"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue">
    <TocEntry
      Title="POST Accounts"
      Link="/Content/3_Service Utilization API/POST Accounts.htm" />
    <TocEntry
      Title="POST Buckets"
      Link="/Content/3_Service Utilization API/POST Buckets.htm" />
    <TocEntry
      Title="POST Users"
      Link="/Content/POST Users.htm" />
    <TocEntry
      Title="POST Service"
      Link="/Content/POST Service.htm" />
    <TocEntry
      Title="UTAPI Metrics and Reporting Granularities"
      Link="/Content/3_Service Utilization API/UTAPI Metrics and Reporting Granularities.htm" />
  </TocEntry>
  <TocEntry
    Title="API Error Handling"
    Link="/Content/5_API Error Handling/API Error Handling.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue">
    <TocEntry
      Title="Error Codes (Client and Server Errors)"
      Link="/Content/5_API Error Handling/Error Codes (Client and Server Errors).htm" />
    <TocEntry
      Title="Service Utilization Error Codes"
      Link="/Content/5_API Error Handling/Service Utilization Error Codes.htm"
      conditions="PrintGuides.GA6.2" />
    <TocEntry
      Title="Error Retries and Exponential Backoff"
      Link="/Content/5_API Error Handling/Error Retries and Exponential Backoff.htm" />
  </TocEntry>
  <TocEntry
    Title="Authenticating API Requests"
    Link="/Content/6_Authenticating API Requests/Authenticating S3 Connector API Requests.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue">
    <TocEntry
      Title="Authentication Methods"
      Link="/Content/6_Authenticating API Requests/Authentication Methods.htm"></TocEntry>
    <TocEntry
      Title="Signature Calculation"
      Link="/Content/6_Authenticating API Requests/Signature Calculation.htm" />
  </TocEntry>
  <TocEntry
    Title="API Knowledge Primers"
    Link="/Content/4_ACL (Access Control List) Primer/S3 Connector API Knowledge Primers.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue">
    <TocEntry
      Title="ACL (Access Control List)"
      Link="/Content/4_ACL (Access Control List) Primer/ACL (Access Control List) Primer.htm"
      BreakType="none"
      StartSection="false"
      PageNumberReset="continue"></TocEntry>
    <TocEntry
      Title="Request Headers"
      Link="/Content/7_Request Headers/Request Headers.htm"
      BreakType="none"
      StartSection="false"
      PageNumberReset="continue"></TocEntry>
    <TocEntry
      Title="Response Headers"
      Link="/Content/8_Response Headers/Response Headers.htm"
      BreakType="none"
      StartSection="false"
      PageNumberReset="continue"
      PageLayout="/Content/Resources/PageLayouts/8_5X11/Chapters8_5X11.flpgl"></TocEntry>
  </TocEntry>
  <TocEntry
    Title="Vault API Reference"
    Link="/Content/9_ Vault API/Vault API Reference.htm"
    BreakType="chapter"
    StartSection="false"
    PageNumberReset="continue"
    SectionNumberReset="continue"
    VolumeNumberReset="same"
    ChapterNumberReset="continue">
    <TocEntry
      Title="Introduction"
      Link="/Content/9_ Vault API/Introduction.htm"
      ChapterNumberReset="continue"
      ChapterNumber="1"
      SectionNumberReset="continue"
      VolumeNumberReset="same"
      StartSection="false" />
    <TocEntry
      Title="vaultclient CLI Commands"
      Link="/Content/9_ Vault API/Vault Client CLI Commands.htm" />
    <TocEntry
      Title="vaultclient Status Codes"
      Link="/Content/9_ Vault API/Status Codes.htm" />
    <TocEntry
      Title="AWS IAM Commands for Users and Groups"
      Link="/Content/9_ Vault API/AWS Commands for Users and Groups.htm" />
    <TocEntry
      Title="AWS IAM Policy Commands"
      Link="/Content/9_ Vault API/AWS IAM Policy Commands.htm" />
    <TocEntry
      Title="Customer Managed Policy Files"
      Link="/Content/9_ Vault API/Customer Managed Policy Files.htm" />
  </TocEntry>
</CatapultToc>