﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="6" MadCap:lastHeight="852" MadCap:lastWidth="648">
    <head>
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">PUT Object ACL</h1>
        <p style="text-align: left;">The PUT Object ACL operation uses the <span class="ElementName">acl</span> subresource to set the access control list (ACL) permissions for an object that exists in a storage system bucket. This operation requires WRITE_ACP permission for the object.</p>
        <p class="Note" style="text-align: left;">WRITE_ACP access is required to set the ACL of an object.</p>
        <p style="text-align: left;">Object permissions are set using one of the following two methods:</p>
        <ul>
            <li>Specifying the ACL in the request body</li>
            <li>Specifying permissions using request headers</li>
        </ul>
        <p style="text-align: left;">Depending on the needs of the application, the ACL may be set on an object using either the request body or the headers.</p>
        <p class="Note" style="text-align: left;">Access permission cannot be specified using both the request body and the request headers.</p>
        <p style="text-align: left;">The ACL of an object is set at the object version level. By default, PUT sets the ACL of the current version of an object. To set the ACL of a different version, use the <span class="ElementName">versionId</span> subresource.</p>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Requests</h2>
        <h3 MadCap:autonum="1.0.1 &#160;">Request Syntax</h3>
        <p style="text-align: left;">The request syntax that follows is for sending the ACL in the request body. If headers are used to specify the permissions for the object,  the ACL cannot be sent in the request body (refer to <MadCap:xref href="../7_Request Headers/Common Request Headers.htm">"Request Headers Detail" on page 1</MadCap:xref> for a list of available headers).</p><pre xml:space="preserve" style="page-break-inside: avoid;">PUT /{{ObjectName}}?acl HTTP/1.1
Host: {{BucketName}}.{{StorageService}}.com
Date: {{date}}
Authorization: {{authorizationString}}</pre><pre xml:space="preserve">

&lt;AccessControlPolicy&gt;
  &lt;Owner&gt;
    &lt;ID&gt;{{iD}}&lt;/ID&gt;
    &lt;DisplayName&gt;{{emailAddress}}&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;{{iD}}&lt;/ID&gt;
        &lt;DisplayName&gt;{{emailAddress}}&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;{{permission}}&lt;/Permission&gt;
    &lt;/Grant&gt;
    ...
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt;</pre>
        <h3 MadCap:autonum="1.0.2 &#160;">Request Parameters</h3>
        <p style="text-align: left;">The PUT Object ACL operation does not use request parameters.</p>
        <h3 MadCap:autonum="1.0.3 &#160;">Request Headers</h3>
        <p style="text-align: left;"> The PUT Object ACL operation can use a number of optional request headers in addition to those that are common to all operations (refer to <MadCap:xref href="../7_Request Headers/Common Request Headers.htm"><MadCap:xref href="../7_Request Headers/Request Headers.htm">"Request Headers" on page&#160;1</MadCap:xref></MadCap:xref>). These request headers are used either to specify a predefined – or <i>canned</i> – ACL, or to explicitly specify grantee permissions.</p>
        <h4>Specifying a Canned ACL</h4>
        <p style="text-align: left;"><MadCap:variable name="S3 Connector Variables.ComponentName" /> supports a set of canned ACLs, each of which has a predefined set of grantees and permissions.</p>
        <p style="text-align: left;">To grant access permissions by specifying canned ACLs, use the <span class="ElementName">x-amz-acl</span> header and specify the canned ACL name as its value. </p>
        <p class="Note" style="text-align: left;"> Other access control specific headers cannot be used when the <span class="ElementName">x-amz-acl</span> header is in use.</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 140px;">
            </col>
            <col class="TableStyle-DetailedwithPadding-Column-Column1">
            </col>
            <col class="TableStyle-DetailedwithPadding-Column-Column1">
            </col>
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">x-amz-acl</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p>Sets the ACL of the object using the specified canned ACL.</p>
                        <p class="InTableFormatted">Default: <span class="Code_Terminal">private</span></p>
                        <p class="InTableFormatted">Valid Values: <span class="Code_Terminal">private</span> | <span class="Code_Terminal">public-read</span> | <span class="Code_Terminal">public-read-write</span> | <span class="Code_Terminal">authenticated-read</span> | <span class="Code_Terminal">bucket-owner-read</span> | <span class="Code_Terminal">bucket-owner-full-control</span></p>
                        <p class="InTableFormatted">Constrains: None</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Explicitly Specifying Grantee Access Permissions</h4>
        <p style="text-align: left;">A set of <span class="ElementName">x-amz-grant-permission</span> headers is available for explicitly granting individualized object access permissions to specific <MadCap:variable name="S3 Connector Variables.ComponentName" /> accounts or groups.</p>
        <p class="Note" style="text-align: left;">Each of the <span class="ElementName">x-amz-grant-permission</span> headers maps to specific permissions the <MadCap:variable name="S3 Connector Variables.ComponentName" /> supports in an ACL.  Please also note that the use of any of these ACL-specific headers negates the use of the <span class="ElementName">x-amz-acl</span> header to set a canned ACL.</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">x-amz-grant-read</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Allows grantee to read the object data and its metadata</p>
                        <p class="InTableFormatted">Default: None </p>
                        <p class="InTableFormatted">Constraints: None</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">x-amz-grant-read-acp</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Allows grantee to read the object ACL</p>
                        <p class="InTableFormatted">Default: None </p>
                        <p class="InTableFormatted">Constraints: None</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">x-amz-grant-write-acp</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Allows grantee to write the ACL for the applicable object</p>
                        <p class="InTableFormatted">Default: None </p>
                        <p class="InTableFormatted">Constraints: None</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">x-amz-grant-full-control</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">Allows grantee the READ, READ_ACP, and WRITE_ACP permissions on the object</p>
                        <p class="InTableFormatted">Default: None </p>
                        <p class="InTableFormatted">Constraints: None</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style="text-align: left;">For each header, the value is a comma-separated list of one or more grantees. Each grantee is specified as a <span class="Code_Terminal">type=value</span> pair, where the type can be one any one of the following:</p>
        <ul>
            <li><span class="Code_Terminal">emailAddress</span> (if value specified is the email address of an account)</li>
            <li><span class="Code_Terminal">id</span> (if value specified is the canonical user ID of an account)</li>
            <li><span class="Code_Terminal">uri</span> (if granting permission to a predefined group)</li>
        </ul>
        <p style="text-align: left;">For example, the following <span class="ElementName">x-amz-grant-read</span> header grants list objects permission to two accounts identified by their email addresses:</p>
        <p class="codeparatext">x-amz-grant-read:  emailAddress="xyz@scality.com", emailAddress="abc@scality.com"</p>
        <h3 MadCap:autonum="1.0.4 &#160;">Request Elements</h3>
        <p style="text-align: left;">If the request body is used to specify an ACL, the following elements must be used.</p>
        <p class="Note" style="text-align: left;">If the request body is requested, the request headers cannot be used to set an ACL.</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 120px;">
            </col>
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 80px;">
            </col>
            <col class="TableStyle-DetailedwithPadding-Column-Column1">
            </col>
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Element</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">AccessControlList</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Container for Grant, Grantee, and Permission</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">AccessControlPolicy</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Contains the elements that set the ACL permissions for an object per grantee
</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">DisplayName</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Screen name of the bucket owner</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grant</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Container for the grantee and his or her permissions</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grantee</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">The subject whose permissions are being set</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">ID</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">ID of the bucket owner, or the ID of the grantee</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Owner</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Container for the bucket owner's display name and ID</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">Permission</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">Specifies the permission given to the grantee</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h4>Grantee Values</h4>
        <p style="text-align: left;">Specify the person (grantee) to whom access rights are being assigned (using request elements):</p>
        <div>
            <ul>
                <li>
                    <p><b>By ID</b>
                    </p>
                    <p class="codeparatext_lastline" style="text-align: left;">&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;&lt;ID&gt;{{ID}}&lt;/ID&gt;&lt;DisplayName&gt;GranteesEmail&lt;/DisplayName&gt;&lt;/Grantee&gt;</p>
                    <p><span class="ElementName">DisplayName</span> is optional and is ignored in the request.</p>
                </li>
            </ul>
        </div>
        <div>
            <ul>
                <li>
                    <p><b>By Email Address</b>
                    </p>
                    <p class="codeparatext_lastline" style="text-align: left;">&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ScalityCustomerByEmail"&gt;&lt;EmailAddress&gt;{{Grantees@email.com}}&lt;/EmailAddress&gt;lt;/Grantee&gt;</p>
                    <p style="text-align: left;">The grantee is resolved to the <span class="ElementName">CanonicalUser</span> and, in a response to a <span class="ElementName">GET Object acl</span> request, appears as the <span class="ElementName">CanonicalUser</span>.</p>
                </li>
            </ul>
        </div>
        <div>
            <ul>
                <li>
                    <p><b>By URI</b>
                    </p>
                    <p class="codeparatext_lastline" style="text-align: left;">&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;&lt;URI&gt;{{http://acs.s3.scality.com/groups/global/AuthenticatedUsers}}&lt;/URI&gt;&lt;/Grantee&gt;</p>
                </li>
            </ul>
        </div>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Responses</h2>
        <h3 MadCap:autonum="1.0.5 &#160;">Response Headers</h3>
        <p class="pgBreakKeepNext" style="text-align: left;">Implementation of the PUT Object ACL operation can include the following response header in addition to the response headers common to all responses (refer to <MadCap:xref href="../8_Response Headers/Response Headers.htm">"Response Headers" on page 1</MadCap:xref>).</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Header</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" MadCap:conditions="PrintGuides.GA">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted" MadCap:conditions="PrintGuides.GA">x-amz-version-id</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted" MadCap:conditions="PrintGuides.GA">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">Returns the version ID of the retrieved object if it has a unique version ID.</p>
                        <p class="InTableFormatted">Default: None</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3 MadCap:autonum="1.0.6 &#160;">Response Elements</h3>
        <p style="text-align: left;">The PUT Object ACL operation does not return response elements.</p>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Examples</h2>
        <h3 MadCap:autonum="1.0.7 &#160;">Grant Access Permission to an Existing Object</h3>
        <p style="text-align: left;">The request sample grants access permission to an existing object, specifying the ACL in the body. In addition to granting full control to the object owner, the XML specifies full control to an account identified by its canonical user ID.</p>
        <h4>Request Sample</h4><pre xml:space="preserve">PUT /my-document.pdf?acl HTTP/1.1
Host: {{bucketName}}.s3.scality.com
Date: Wed, 28 Oct 2009 22:32:00 GMT
Authorization: {{authorizationString}}
Content-Length: 124

&lt;AccessControlPolicy&gt;
  &lt;Owner&gt;
    &lt;ID&gt;75aa57f09aa0c8caeab4f8c24e99d10f8e7faeebf76c078efc7c6caea54ba06a&lt;/ID&gt;
    &lt;DisplayName&gt;{{customersName}}@scality.com&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;75aa57f09aa0c8caeab4f8c24e99d10f8e7faeeExampleCanonicalUserID&lt;/ID&gt;
        &lt;DisplayName&gt;{{customersName}}@scality.com&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;FULL_CONTROL&lt;/Permission&gt;
    &lt;/Grant&gt;
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt;</pre>
        <h4>Response Sample</h4><pre xml:space="preserve">HTTP/1.1 200 OK
x-amz-id-2: eftixk72aD6Ap51T9AS1ed4OpIszj7UDNEHGran
x-amz-request-id: 318BC8BC148832E5
x-amz-version-id: 3/L4kqtJlcpXrof3vjVBH40Nr8X8gdRQBpUMLUo
Date: Wed, 28 Oct 2009 22:32:00 GMT
Last-Modified: Sun, 1 Jan 2006 12:00:00 GMT
Content-Length: 0
Connection: close
Server: ScalityS3
Setting the AC</pre>
        <h3 MadCap:autonum="1.0.8 &#160;">Setting the ACL of a Specified Object Version</h3>
        <p style="text-align: left;">The request sample sets the ACL on the specified version of the object.</p>
        <h4>Request Sample</h4><pre xml:space="preserve">PUT /my-document.pdf?acl&amp;amp;versionId=3HL4kqtJlcpXroDTDmJ+rmSpXd3dIbrHY+MTRCxf3vjVBH40Nrjfkd HTTP/1.1
Host: {{bucketName}}.s3.scality.com
Date: Wed, 28 Oct 2009 22:32:00 GMT
Authorization: {{authorizationString}}
Content-Length: 124

&lt;AccessControlPolicy&gt;
  &lt;Owner&gt;
    &lt;ID&gt;75aa57f09aa0c8caeab4f8c24e99d10f8e7faeebf76c078efc7c6caea54ba06a&lt;/ID&gt;
    &lt;DisplayName&gt;mtd@scality.com&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;75aa57f09aa0c8caeab4f8c24e99d10f8e7faeebf76c078efc7c6caea54ba06a&lt;/ID&gt;
        &lt;DisplayName&gt;mtd@scality.com&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;FULL_CONTROL&lt;/Permission&gt;
    &lt;/Grant&gt;
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt;</pre>
        <h4>Response Sample</h4><pre xml:space="preserve">HTTP/1.1 200 OK
x-amz-id-2: eftixk72aD6Ap51u8yU9AS1ed4OpIszj7UDNEHGran
x-amz-request-id: 318BC8BC148832E5
x-amz-version-id: 3/L4kqtJlcpXro3vjVBH40Nr8X8gdRQBpUMLUo
Date: Wed, 28 Oct 2009 22:32:00 GMT
Last-Modified: Sun, 1 Jan 2006 12:00:00 GMT
Content-Length: 0
Connection: close
Server: ScalityS3</pre>
        <h3 MadCap:autonum="1.0.9 &#160;">Access Permissions Specified Using Headers</h3>
        <p style="text-align: left;">The request sample uses ACL-specific request header <span class="ElementName">x-amz-acl</span>, and specifies a canned ACL (<span class="Code_Terminal">public_read</span>) to grant object read access to everyone.</p>
        <h4>Request Sample</h4><pre xml:space="preserve">PUT ExampleObject.txt?acl HTTP/1.1
Host: {{bucketName}}.s3.scality.com
x-amz-acl: public-read
Accept: */*
Authorization: {{authorizationString}}
Host: s3.scality.com
Connection: Keep-Alive</pre>
        <h4>Response Sample</h4><pre xml:space="preserve">HTTP/1.1 200 OK
x-amz-id-2: w5YegkbG6ZDsje4WK56RWPxNQHIQ0CjrjyRVFZhEJI9E3kbabXnBO9w5G7Dmxsgk
x-amz-request-id: C13B2827BD8455B1
Date: Sun, 29 Apr 2012 23:24:12 GMT
Content-Length: 0
Server: ScalityS3</pre>
    </body>
</html>