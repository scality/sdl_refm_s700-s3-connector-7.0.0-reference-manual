﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="6" MadCap:lastHeight="869" MadCap:lastWidth="648">
    <head>
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>Signature Calculation</h1>
        <p style="text-align: left;">Authentication information sent in a request must include a <span class="ProprietaryNonConventional">signature</span>, a 256-bit string expressed as 64 lowercase hexadecimal characters. For example:</p>
        <p class="codeparatext_lastline">fe5f80f77d5fa3beca038a248ff027d0445342fe2855ddc963176630326f1024</p>
        <p style="text-align: left;">The signature is calculated from selected elements of a request, so it will vary from request to request.</p>
        <p style="text-align: left;">For both V2 Authentication and V4 Authentication, the first step in calculating a signature is the concatenation of select request elements to form a string (referred to as the <span class="ProprietaryNonConventional">StringToSign</span>). Thereafter, the process for producing the signature differs, depending on the authentication method in use. </p>
        <h2>V2 Authorization Signatures</h2>
        <p style="text-align: left;">V2 Authorization uses the <span class="ElementName">sha1</span> encryption algorithm along with an account secret key to encrypt a <span class="ElementName">StringToSign</span>, which results in the signature.   The request contains the signature and the elements used to build the StringToSign. The server receiving the request takes the element from the request and builds the StringToSign itself.  It then pulls the account secret key from Vault (based on the accessKey in the request) and calculates the signature.  If the two signatures match, the requester has the correct secretKey and the request is authorized.</p>
        <h2>V4 Authorization Signatures</h2>
        <p class="pgBreakKeepNext" style="text-align: left;">V4 Authorization is similar to V2 Authorization, though <span class="ProprietaryNonConventional">sha256</span> is used  as the encryption algorithm in lieu of <span class="ProprietaryNonConventional">sha1</span>.  Also, a varying combination of request elements are used to build the <span class="ElementName">StringToSign</span> (including a hash of the payload in the case of a put request), and instead of just using the secret key a signing key is used to create the actual signature.</p>
        <p style="text-align: center;">
            <img src="../Resources/Images/signing-overview.png" class="OneHundredPercent">
            </img>
        </p>
        <p style="text-align: left;">Upon receiving an authenticated request, <MadCap:variable name="S3 Connector Variables.ComponentName" /> re-creates the signature using the authentication information that is contained in the request. If the signatures match, <MadCap:variable name="S3 Connector Variables.ComponentName" /> processes the request; otherwise, the request is rejected.</p>
        <p style="text-align: left;">V4 Authorization signature calculations vary depending on the method chosen for transferring the request payload. S3 Connector supports payload transfer in both a single chunk, as well as in multiple chunks.</p>
        <h3>Transfer Payload in a Single Chunk</h3>
        <p style="text-align: left;">Two calculation options area available for transferring payload in a single chunk, signed and unsigned.</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 160px;" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 482px;" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Calculation Option</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Signed Payload</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Optionally compute the entire payload checksum and include it in signature calculation, whihc provides added security but requires that the payload be read twice or be buffered in memory.</p>
                        <p class="InTableFormatted">For example, in order to upload a file, you need to read the file first to compute a payload hash for signature calculation and again for transmission when you create the request. For smaller payloads, this approach might be preferable. However, for large files, reading the file twice can be inefficient, so you might want to upload data in chunks instead.</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">Unsigned Payload</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">To not include payload checksum in signature calculation. </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3>Transfer Payload in Multiple Chunks</h3>
        <p style="text-align: left;"> With a chunked upload, the payload is transferred in chunks, which can be performed regardless of the payload size. This method circumvents the need to read the entire payload to calculate the signature. Instead, for the first chunk, a seed signature is calculated that uses only the request headers. The second chunk contains the signature for the first chunk, and each subsequent chunk contains the signature for the chunk that precedes it. At the end of the upload, a final chunk is sent with 0 bytes of data that contains the signature of the last chunk of the payload. </p>
        <p style="text-align: left;">When a request is sent, <MadCap:variable name="S3 Connector Variables.ComponentName" /> must be informed which of the preceding options has been chosen for signature calculation, by adding the <span class="ElementName">x-amz-content-sha256</span> header with one of the following values:</p>
        <ul>
            <li>STREAMING-AWS4-HMAC-SHA256-PAYLOAD (if chunked upload options are selected).</li>
            <li> payload checksum (signed payload option), or the literal string for the unsigned payload option UNSIGNED-PAYLOAD (if the choice is made to upload payload in a single chunk)</li>
        </ul>
        <p style="text-align: left;">Upon receiving the request, <MadCap:variable name="S3 Connector Variables.ComponentName" /> recreates the <span class="ProprietaryNonConventional">StringToSign</span> using information in the Authorization header and the date header. It then verifies with authentication service that the signatures match. The request date can be specified by using either the HTTP Date or the <span class="ElementName">x-amz-date</span> header. If both headers are present, <span class="ElementName">x-amz-date</span> takes precedence.</p>
        <p style="text-align: left;">If the signatures match, Amazon S3 processes your request; otherwise, the request will fail.</p>
    </body>
</html>